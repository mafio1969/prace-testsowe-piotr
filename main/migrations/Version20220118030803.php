<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220118030803 extends AbstractMigration
{
    public const DATABASE_SQL_FILE_NAME = 'struktura.sql';

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $sqlStatement = file_get_contents(
            '/main/migrations/data/'.self::DATABASE_SQL_FILE_NAME
        );

        $this->connection->executeStatement($sqlStatement);
    }

    public function down(Schema $schema): void
    {
        // this down() tantamount to drop the database
    }
}

